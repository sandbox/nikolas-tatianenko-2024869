<?php

/**
 * @file
 * Settings page.
 */

/**
 * Settings page form.
 *
 * Page contains settings of cart. Like modal window,float cart,
 * and cart button names.
 *
 * @param $form
 *   Form structure.
 * @param $form_state
 *   Form state.
 *
 * @return
 *   Form.
 */
function advanced_commerce_cart_ajax_settings_page($form, &$form_state) {
  $form['modal_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modal window settings'),
    '#description' => '',
  );
  $form['modal_settings']['message'] = array(
    '#type' => 'markup',
    '#markup' => t('If you use languages, different from English we recommend to write words in english, and translate it to yours on translation page'),
  );
  $options = array(
    'none' => t('none'),
    'standalone' => t('Standalone modal window script'),
  );
  $form['modal_settings']['advanced_commerce_cart_ajax_modal_window_type'] = array(
    '#default_value' => variable_get('advanced_commerce_cart_ajax_modal_window_type', ADVANCED_COMMERCE_CART_AJAX_MODAL_WINDOW_TYPE),
    '#type' => 'radios',
    '#options' => $options,
  );
  $form['cart_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ajax cart settings'),
    '#description' => '',
  );
  $form['cart_settings']['advanced_commerce_cart_ajax_floating_cart'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('advanced_commerce_cart_ajax_floating_cart', ADVANCED_COMMERCE_CART_AJAX_FLOATING_CART),
    '#title' => t('Floating cart'),
    '#description' => '',
  );

  return system_settings_form($form);
}
