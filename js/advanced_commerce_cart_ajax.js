/**
 * @file
 * Javascript for module.
 */

jQuery(document).ready(function () {
  jQuery("#commerce_advanced_ajax_cart_content_close").live("click", function () {
    jQuery(this).parent().parent().parent().hide();
  });

  if (Drupal.settings.advanced_commerce_cart_ajax['floating-block']) {
    if (jQuery('div').is('#block-commerce-cart-cart')) {
      var box = jQuery('#block-commerce-cart-cart');
      var top = box.offset().top;
      console.log(box);
      console.log(top);
      jQuery(window).scroll(function () {
        var windowpos = jQuery(window).scrollTop();
        jQuery('.floating_cart').hover(function () {
          box.css({opacity: 1});
        });
        if (windowpos < top + 20) {
          var width = box.css('width');
          box.css({position: 'static', marginTop: 'auto', opacity: '1'});
        }
        else {
          box.addClass('floating_cart');
          box.css({top: '20px', position: 'fixed', marginTop: '0', opacity: '0.5'});
        }
      });
    }
  }
});
