<?php
/**
 * @file
 * Modal window template.
 *
 * @ingroup advanced_commerce_cart_ajax_templates
 */
?>

<div id="commerce_advanced_ajax_cart_wrapper">
  <div id="caac_bg" style="height: 2833px;"></div>
  <div id="commerce_advanced_ajax_cart_content">
    <h4><?php print check_plain($product->title); ?></h4>
    <span id="alert_text"><?php print $message; ?></span>
    <h5><?php print l(t('Cart'), 'cart'); ?></h5>
    <hr>
    <span id="commerce_advanced_ajax_cart_content_button"><button
        id="commerce_advanced_ajax_cart_content_close"><?php print t('ОК'); ?></button></span>
  </div>
</div>
