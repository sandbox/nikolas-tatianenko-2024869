<?php
/**
 * @file
 * Cart wrapper template.
 *
 * @ingroup advanced_commerce_cart_ajax_templates
 */
?>
<div class="<?php print $class; ?>"><?php print $cart; ?></div>
