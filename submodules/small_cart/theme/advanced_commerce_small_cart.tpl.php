<?php
/**
 * @file
 * Small cart template.
 *
 * @ingroup advanced_commerce_cart_ajax_templates
 */
?>
<div id="simple-commerce-cart-wrapper">
  <div id="small-cart" class="small-cart">
    <a class="" id="c-cart" href="<?php $cart['cart_url']; ?>"> <?php print t('Cart'); ?><span
        class='quantity'>( <?php print $cart['quantity']; ?>)</span></a>
      <?php if ($cart['quantity'] > 0) : ?>
      <div style="visibility: visible;" id="c-price"><?php print t('Total'); ?><em
          class="price"><?php print $cart['summ']; ?></em></div>
      <?php endif; ?>
  </div>
</div>
