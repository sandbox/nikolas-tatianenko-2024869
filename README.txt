Advanced commerce cart ajax  for Drupal 7.x-1.x
  by Nikolas Tatianenko


-- SUMMARY --

 The Advanced commerce cart ajax module provides the ajax cart 
 and simple ajax cart for Drupal Commerce. This module extends functionality
 of standart add to cart button. 
 It gives possibility of update cart block without refreshing the page.

 For a full description of the module, visit the project page:
 https://drupal.org/sandbox/nikolas-tatianenko/2024869

 To submit bug reports and feature suggestions, or to track changes:
 https://drupal.org/project/issues/2024869

-- FUNCTIONALITY --

  Refreshing cart without refreshing the page;
* Open modal window with additional ingo after clicking on the add 
  to cart button;
* adding the simple cart block
* simple cart block,modal window has there own template

-- REQUIREMENTS --

* Drupal Commerce
* Drupal Commerce Cart
* Views

-- INSTALLATION --

* Install as usual, 
  see https://drupal.org/documentation/install/modules-themes/modules-7 for 
  further information.

* Put the module in your drupal modules directory and enable it 
  in admin/modules. 

* Go to Store -> Configuration-> advanced ajax cart settings  or type url  
  http://example.com/admin/commerce/config/advanced_commerce_cart_ajax and 
  configure module enable 
  simple ajax cart block, if you need this functionality

-- CONFIGURATION --

* Configure modal window
  
  Visit http://example.com/admin/commerce/config/advanced_commerce_cart_ajax 
  and enable modal window checkbox

* Configure floating cart
  
  Visit  http://example.com/admin/commerce/config/advanced_commerce_cart_ajax 
  and enable floating block


-- CONTACT --

   Nikolas Tatianenko - https://drupal.org/user/1579942
