<?php

/**
 * @file
 * Functions for module.
 */

/**
 * Ajax callback.
 *
 * Main function of module, calculating content of new cart block and
 * asynchronous add it to page, opening modal window and forking with
 * the small cart.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 *   Ajax command.
 */
function advanced_commerce_cart_ajax_ajax_form_callback($form, &$form_state) {
  $user = $GLOBALS['user'];
  $order_id = commerce_cart_order_id($user->uid);
  $product = array();
  $cart = commerce_embed_view('commerce_cart_block', 'default', array($order_id));
  $commands = array();
  // Wrap element.
  $cart = theme('advanced_commerce_cart_ajax_wrapper', array('cart' => $cart));

  if (module_exists('advanced_commerce_small_cart_ajax')) {
    $commands[] = ajax_command_replace('#simple-commerce-cart-wrapper', theme('advanced_commerce_small_cart',
      array('cart' => advanced_commerce_small_cart_ajax_simple_commerce_cart())));
  }
  // Updating cart.
  $commands[] = ajax_command_replace('.' . ADVANCED_COMMERCE_CART_AJAX_CART_WRAPPER, $cart);
  $message = t("Product in cart");

  // Calling the modal window.
  $modal_window_type = variable_get('advanced_commerce_cart_ajax_modal_window_type', ADVANCED_COMMERCE_CART_AJAX_MODAL_WINDOW_TYPE);
  if ($modal_window_type == 'standalone') {
    $commands[] = ajax_command_append('body', theme('advanced_commerce_cart_ajax_add_message', array(
      'product' => $product,
      'message' => $message,
    )));
  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Ajax callback submit.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function advanced_commerce_cart_ajax_ajax_form_callback_rebuild_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Product list.
 *
 * Showing the list of products for current products.
 *
 * @return array
 *   User product list.
 */
function advanced_commerce_cart_ajax_user_product_list() {
  $list = array();
  $user = $GLOBALS['user'];
  if ($order = commerce_cart_order_load($user->uid)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $wrapper->commerce_line_items->value();
    foreach ($line_items as $line_item) {
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      $list[$product->product_id] = $product->product_id;
    }
  }
  return $list;
}
